/*

This file is part of GShare.

GShare is free software; you can redistribute it 
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

GShare  is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GShare; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The Initial Developer of the Original Code is Celso Pinto.
Portions created by Celso Pinto is Copyright (C) 2006. 

All Rights Reserved.

*/

using System;

namespace GShare
{
    public interface IPermissionManager
    {
        bool AllowWrite { get; set;}
        bool AllowDelete { get; set;}    
    }
    
    public interface ISettingsManager : IPermissionManager
    {
        bool SharingEnabled { get; set;}
        bool AllowAnonymous { get; set;}

        string Username { get; set;}
        string Password { get; set;}

        string ShareRoot { get; }

        bool IsFirstTime { get; }
        string ShareName { get; set; }
        int ServerPort { get;set; }
    }

    public interface IGShareDaemon : ISettingsManager,IAuthenticationManager
    {
        void Shutdown();
    }

    public interface IAuthenticationManager
    {
        bool AuthenticateUser(string username, string password);
    }
}
