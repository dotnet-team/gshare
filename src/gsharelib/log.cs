using System;
using System.Diagnostics;

using GLib;

namespace GShare
{

    public sealed class Log
    {

        private static GLib.Log iLog = new GLib.Log();
        
        private Log()
        {}
    
        [ConditionalAttribute("DEBUG")]
        public static void DebugMessage(string message, params object[] args)
        {
            iLog.WriteLog(null, GLib.LogLevelFlags.Debug, message, args);
        }
        
        public static void ErrorMessage(string message, params object[] args)
        {
            iLog.WriteLog(null, GLib.LogLevelFlags.Critical, message, args);
        }
    
    }
    
}
