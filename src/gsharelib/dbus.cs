/*

This file is part of GShare.

GShare is free software; you can redistribute it 
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

GShare  is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GShare; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The Initial Developer of the Original Code is Celso Pinto.
Portions created by Celso Pinto is Copyright (C) 2006. 

All Rights Reserved.

*/

using System;

using NDesk.DBus;
using org.freedesktop.DBus;

namespace GShare
{

    [Interface ("com.yimports.gshare.Service")]
    public interface IGShareService
    {
        void SetAllowAnonymousLogin(bool allowAnonymous);
        bool GetAllowAnonymousLogin();
        void SetLogin(string username,string password);
        string GetUsername();
        string GetPassword();
        void SetPermissions(bool allowWrite, bool allowDelete);
        bool GetAllowWrite();
        bool GetAllowDelete();
        bool SetSharingEnabled(bool enabled);
        bool GetSharingEnabled();
        string GetShareName();
        void SetShareName(string name);
        int GetServerPort();
        void SetServerPort(int port);
        void Shutdown();
        bool IsFirstTime();
        double GetDaemonVersion();

    }
    
    public class GShareService : IGShareService
    {
        private const string 
            BUS_NAME = "com.yimports.gshare",
            DBUS_OBJ_PATH = "/com/yimports/gshare/Service";
        
        private static GShareService self = null;
        private IGShareDaemon iDaemon = null;

        private GShareService()
        {
        }
        
        public static bool ServiceRunning()
        {
            return Bus.Session.NameHasOwner(BUS_NAME);        
        }
        
        public static IGShareService FindDBusInstance()
        {
            if (!ServiceRunning())
            {
                try
                {
                    Bus.Session.StartServiceByName(BUS_NAME);
                }
                catch
                {
                    /* failed to start service */
                    return null;
                }
            }
            
            try
            {
                return Bus.Session.GetObject<IGShareService>(BUS_NAME,new ObjectPath(DBUS_OBJ_PATH));
            }
            catch
            {
                return null;
            }
            
        }
        
        public static GShareService GetInstance()
        {
            if (self == null)
                self = new GShareService();
            
            return self;
            
        }

        public void SetDaemon(IGShareDaemon daemon)
        {
            iDaemon = daemon;
        }
        
        public void StartPublishing()
        {
            if (ServiceRunning())
                return;
                
            Bus.Session.Register(BUS_NAME,new ObjectPath(DBUS_OBJ_PATH),self);
            Bus.Session.RequestName(BUS_NAME);
        }

        public void StopPublishing()
        {
            if (!ServiceRunning())
                return;
                
            Bus.Session.ReleaseName(BUS_NAME);
            Bus.Session.Unregister(BUS_NAME,new ObjectPath(DBUS_OBJ_PATH));
        }

        public void SetAllowAnonymousLogin(bool allowAnonymous)
        {
            iDaemon.AllowAnonymous = allowAnonymous;
        }

        public bool GetAllowAnonymousLogin()
        {
            return iDaemon.AllowAnonymous;
        }

        public void SetLogin(string username,string password)
        {
            iDaemon.Username = username;
            iDaemon.Password = password;
        }

        public string GetUsername()
        {
            return iDaemon.Username;
        }

        public string GetPassword()
        {
            return iDaemon.Password;
        }

        public void SetPermissions(bool allowWrite, bool allowDelete)
        {
            iDaemon.AllowWrite = allowWrite;
            iDaemon.AllowDelete = allowDelete;
        }

        public bool GetAllowWrite()
        {
            return iDaemon.AllowWrite;
        }

        public bool GetAllowDelete()
        {
            return iDaemon.AllowDelete;
        }

        public bool SetSharingEnabled(bool enabled)
        {
            iDaemon.SharingEnabled = enabled;
            
            /*  if the user tried to enable the service 
                but the service failed to start
                return false
            */
            if (enabled && !iDaemon.SharingEnabled)
                return false;
            
            /* in every other case, return true */
            return true;
        }

        public bool GetSharingEnabled()
        {
            return iDaemon.SharingEnabled;
        }

        public void Shutdown()
        {
            iDaemon.Shutdown();
        }

        public bool IsFirstTime()
        {
            return iDaemon.IsFirstTime;
        }

        public string GetShareName()
        {
            return iDaemon.ShareName;
        }

        public void SetShareName(string name)
        {
            iDaemon.ShareName = name;
        }
        public int GetServerPort()
        {
            return iDaemon.ServerPort;
        }

        public void SetServerPort(int port)
        {
            iDaemon.ServerPort = port;
        }
        
        public double GetDaemonVersion()
        {
            return Utility.GShareVersion;
        }
    }
        
}
