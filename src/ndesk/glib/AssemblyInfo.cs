using System.Reflection;
using System.Runtime.CompilerServices;

//[assembly: AssemblyVersion("0.0.0.*")]
[assembly: AssemblyTitle ("NDesk.DBus.GLib")]
[assembly: AssemblyDescription ("GLib integration for NDesk.DBus, the D-Bus IPC library")]
[assembly: AssemblyCopyright ("Copyright (C) Alp Toker")]
[assembly: AssemblyCompany ("NDesk")]
