/*

This file is part of GShare.

GShare is free software; you can redistribute it 
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

GShare  is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GShare; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The Initial Developer of the Original Code is Celso Pinto.
Portions created by Celso Pinto is Copyright (C) 2006. 

All Rights Reserved.

*/

using System;
using System.IO;
using System.Threading;

using Gtk;
using GConf;
using Mono.Unix;
using Avahi;
using NDesk.DBus;

using GShare.FTP;

namespace GShare
{
    public class GShared : IGShareDaemon
    {
        private readonly string 
            HOME_SHARE = Catalog.GetString("~/Shared Files");

        private const string
            AVAHI_DOMAIN = "local",
            AVAHI_PROTOCOL = "_ftp._tcp";
        
        private readonly string
            AVAHI_SERVICE_NAME = Catalog.GetString("{0}'s Shared Files");

        private const string
            GCONF_SHARE_ENABLED = "/apps/gshare/enabled",
            GCONF_ALLOW_ANONYMOUS = "/apps/gshare/allow_anonymous",
            GCONF_ALLOW_WRITE = "/apps/gshare/allow_write",
            GCONF_ALLOW_DELETE = "/apps/gshare/allow_delete",
            GCONF_USERNAME = "/apps/gshare/username",
            GCONF_PASSWORD = "/apps/gshare/password",
            GCONF_FIRST_TIME = "/apps/gshare/first_time",
            GCONF_SHARE_DIR = "/apps/gshare/share_dir",
            GCONF_PORT = "/apps/gshare/port",
            GCONF_SHARE_NAME = "/apps/gshare/share_name";
            
        private const int DEFAULT_SERVER_PORT = 10021;            

        private FTPServer iFtpServer = null;
        private GShareService iDbusService = null;

        private GConf.Client iGconf = null;
        private Avahi.Client iAvahiClient = null;
        private Avahi.EntryGroup iEntryGroup = null;
        private string iAvahiServiceName = null;

        /* simple state */
        private bool iDaemonRunning = false;

        /* properties */
        public string ShareName
        {
            get
            {
                string shareName = "";
                try
                {
                    shareName = (string)iGconf.Get(GCONF_SHARE_NAME);
                }
                catch(GConf.NoSuchKeyException ex)
                {
                }

                if (shareName.Trim() == "")
                {
                    string username = null;
                    UnixUserInfo user = Mono.Unix.UnixUserInfo.GetRealUser();
                    if (user.RealName != null && user.RealName.Length > 0)
                        username = user.RealName.Split(new char[]{','})[0];

                    if (username == null || username == "")
                    {
                        /*this happens on ArchLinux*/
                        shareName = String.Format(AVAHI_SERVICE_NAME, Mono.Unix.UnixEnvironment.UserName);
                    }
                    else
                        shareName = String.Format(AVAHI_SERVICE_NAME, username);

                    iGconf.Set(GCONF_SHARE_NAME,shareName);
                }

                return shareName;

            }
            set 
            {
                string oldValue = ShareName;
                iGconf.Set(GCONF_SHARE_NAME,value); 
                if (oldValue != value)
                {
                    iAvahiServiceName = value;
                    StopZeroConf();
                    StartZeroConf();
                }
            }
        }
        public bool IsFirstTime
        {
            get
            {
                bool firstTime = false;
                try
                {
                    firstTime = (bool)iGconf.Get(GCONF_FIRST_TIME);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    firstTime = true;
                }
                
                if (firstTime)
                    iGconf.Set(GCONF_FIRST_TIME,false);
                
                /* Not first time if ShareRoot exists */
                if (Directory.Exists(ShareRoot))
                    firstTime = false;
                
                return firstTime;
            }
        }

        public bool SharingEnabled
        {
            get
            {
                try
                {
                    return (bool)iGconf.Get(GCONF_SHARE_ENABLED);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    SharingEnabled = false;
                    return false;
                }
            }

            set 
            {

                bool reallyEnable = value;
                if (value)
                    reallyEnable = StartDaemon();
                else
                    StopDaemon();
                
                iGconf.Set(GCONF_SHARE_ENABLED, reallyEnable);
                
                /* if sharing is enabled, autostart the service */
                if (Utility.GnomeVersion >= 2.14)
                    StartAfterLogin = reallyEnable;
                
            }
        }

        public bool AllowAnonymous
        {
            get
            {
                try
                {
                    return (bool)iGconf.Get(GCONF_ALLOW_ANONYMOUS);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    AllowAnonymous = true;
                    return true;
                }
            }

            set { iGconf.Set(GCONF_ALLOW_ANONYMOUS, value); }
        }

        public int ServerPort
        {
            get
            {
                try
                {
                    return (int)iGconf.Get(GCONF_PORT);
                }
                catch(GConf.NoSuchKeyException ex)
                {
                    iGconf.Set(GCONF_PORT,DEFAULT_SERVER_PORT);
                    return DEFAULT_SERVER_PORT;
                }
            }
            set 
            { 
                int oldValue = ServerPort;
                iGconf.Set(GCONF_PORT,value); 
                if (oldValue != value)
                {
                    /* 
                        just restart the whole daemon instead of just 
                        restarting the FTP server and the ZeroConf client 
                    */
                    StopDaemon();
                    StartDaemon();
                }
            }
        }

        public bool AllowWrite
        {
            get
            {
                try
                {
                    return (bool)iGconf.Get(GCONF_ALLOW_WRITE);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    AllowWrite = false;
                    return false;
                }
            }

            set { iGconf.Set(GCONF_ALLOW_WRITE, value); }
        }

        public bool AllowDelete
        {
            get
            {
                try
                {
                    return (bool)iGconf.Get(GCONF_ALLOW_DELETE);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    AllowDelete = false;
                    return false;
                }
            }

            set { iGconf.Set(GCONF_ALLOW_DELETE, value); }
        }

        public String Username
        {
            get
            {
                try
                {
                    return (string)iGconf.Get(GCONF_USERNAME);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    AllowAnonymous = true;
                    Username = "";
                    return "";
                }
            }

            set { iGconf.Set(GCONF_USERNAME, value); }
        }

        public String Password
        {
            get
            {
                try
                {
                    return (string)iGconf.Get(GCONF_PASSWORD);
                }
                catch (GConf.NoSuchKeyException ex)
                {
                    AllowAnonymous = true;
                    Password = "";
                    return "";
                }
            }

            set { iGconf.Set(GCONF_PASSWORD, value); }
        }

        public String ShareRoot
        {
            get 
            {
                try
                {
                    return Utility.GetCanonicalShareRootPath( (string)iGconf.Get(GCONF_SHARE_DIR) );
                }
                catch(GConf.NoSuchKeyException ex)
                {
                    iGconf.Set(GCONF_SHARE_DIR,HOME_SHARE);
                    return Utility.GetCanonicalShareRootPath(HOME_SHARE);
                }
            }
        }

        private bool StartAfterLogin
        {
            get 
            {
                return File.Exists(String.Concat(Utility.XdgAutoStartDir,
                                                    Constants.AUTOSTART_FILE));
            }
            set 
            {
                if (value)
                {
                     
                    string xdgDir = Utility.XdgAutoStartDir;
                    string autostartFilePath = Utility.DaemonAutoStartFilePath;
                     
                    if (File.Exists(String.Concat(xdgDir,Constants.AUTOSTART_FILE)))
                    {
                        /*only copy if file doesn't exist. This saves me a world of pain */
                        return;
                    }
                    
                    Log.DebugMessage("Copying autostart file to {0}",xdgDir);
                     
                    if (!File.Exists(autostartFilePath))
                    {
                        Log.DebugMessage("Autostart file not found at {0}",
                                            autostartFilePath);
                        return;
                    }
                    
                    if (!Directory.Exists(xdgDir))
                        Directory.CreateDirectory(xdgDir);
                     
                    File.Copy(  autostartFilePath,
                                String.Concat(xdgDir,Constants.AUTOSTART_FILE));

                 }
                 else
                 {
                     File.Delete(String.Concat( Utility.XdgAutoStartDir,
                                                Constants.AUTOSTART_FILE));
                 }
            }
        }

        private GShared()
        {

            iGconf = new GConf.Client();

            iDbusService = GShareService.GetInstance();

        }

        private void OnFTPServerShutdown(FTPServerShutdownReason reason)
        {
            if (reason == FTPServerShutdownReason.Error)
            {
                Log.ErrorMessage("Shutting down FTP server because of an error");
                Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate
                    { 
                        Utility.ShowGenericFatalErrorDialog(); 
                        Shutdown(); 
                    }));
            }
        }
        
        public bool AuthenticateUser(string username, string password)
        {
            return ((String.Compare(username,"anonymous",true) == 0 && AllowAnonymous) ||
                    (String.Compare(username,Username,true) == 0 &&
                     String.Compare(password,password,true) == 0));
        }

        private void PublishFTPServer()
        {
            Log.DebugMessage("PublishFTPServer() IN, daemonRunning: {0}",
                                iDaemonRunning);

            if (iDaemonRunning)
                return;

            iAvahiServiceName = ShareName;
            
            iFtpServer.StartServer();
            iDaemonRunning = true;
            
            StartZeroConf();
            
        }

        private void OnClientStateChange(object obj, ClientStateArgs args)
        {

            Log.DebugMessage("OnClientStateChange: {0}", args.State);

            switch(args.State)
            {
                case ClientState.Collision:
                    /*TODO figure out what to do*/
                    Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate
                        { 
                            StopDaemon();
                        }));
                    break;
                case ClientState.Failure:
                    Log.ErrorMessage("Zeroconf client failed to register");
                    Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate
                        {
                            Utility.ShowCannotPublishServiceDialog(); 
                            Shutdown(); 
                        }));
                    break;
                case ClientState.Running:
                    Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate
                        {
                            PublishFTPServer(); 
                        }));
                    break;
            }

        }

        private void OnEntryGroupStateChanged(object o, EntryGroupStateArgs args)
        {

            Log.DebugMessage("OnEntryGroupStateChanged: {0}", args.State);

            switch(args.State)
            {
                case EntryGroupState.Collision:
                    if (iAvahiServiceName == ShareName)
                    {
                        /* just collided, append the hostname to the service name */
                        iAvahiServiceName = String.Concat(iAvahiServiceName,
                                                        String.Format(" @ {0}",Environment.MachineName));
                    }
                    else
                    {
                        /* already has hostname in service name, use something else */
                        iAvahiServiceName = EntryGroup.GetAlternativeServiceName(iAvahiServiceName);
                    }
                    
                    Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate
                        {
                            StartZeroConf();
                        }));
                    
                    break;
                case EntryGroupState.Failure:
                    Log.ErrorMessage("Failed to publish FTP service on zeroconf network");
                    Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate
                        {
                            Utility.ShowCannotPublishServiceDialog();
                            Shutdown(); 
                        }));
                    break;

                case EntryGroupState.Established:
                    Log.DebugMessage("Service published");
                    break;

            }

        }

        private void StartZeroConf()
        {

            /* we should make sure that client is running */
            /* iAvahiClient.State, if not running, popup error message */
            try
            {

                iEntryGroup = new EntryGroup(iAvahiClient);
                iEntryGroup.StateChanged += OnEntryGroupStateChanged;

                iEntryGroup.AddService( iAvahiServiceName, 
                                        AVAHI_PROTOCOL, 
                                        AVAHI_DOMAIN, 
                                        (ushort)ServerPort);
                                        
                iEntryGroup.Commit();

                Log.DebugMessage("Listening at {0}",ServerPort);

            }
            catch (Exception ex)
            {
                Log.ErrorMessage("Failed to publish FTP Server, shutting down application.\nException: {0}\n{1}",
                                    ex.Message,
                                    ex.StackTrace);
                                
                Utility.InvokeLater((GShare.Utility.BackgroundTask)(delegate 
                    {
                        Utility.ShowCannotPublishServiceDialog(); 
                        Shutdown(); 
                    }));
                
            }

        }

        private void StopZeroConf()
        {
            if (iEntryGroup == null) return;

            Log.DebugMessage("StopZeroConf IN");

            try
            {
            
                iEntryGroup.Reset();
                iEntryGroup.Dispose();
                
            }
            catch { /* NOFAIL*/ }
        
            iEntryGroup = null;
            
        }
        
        private bool StartDaemon()
        {

            /*
                first try to connect to avahi-daemon
                if this isn't possible, maybe because the daemon isn't
                running, there's no point in starting the internal FTP server.

                If the client cannot connect, show warning dialog to the user.

                some may argue that starting the FTP Server shouldn't depend
                on being able to publish the service on the network but
                the whole purpose of this application is to provide and publish
                a FTP server so I want to tie this together.
            */
            try
            {
                iAvahiClient = new Avahi.Client();
                iAvahiClient.StateChanged += OnClientStateChange;

                Log.DebugMessage("AvahiClient connected");

            }
            catch(Avahi.ClientException ex)
            {
                /* 
                    More often than not, this exception is caught because
                    the Avahi.Client didn't manage to connect to the 
                    avahi-daemon.
                    
                    One option is to allow the daemon to run nonetheless but 
                    then this is quite confusing. Even if the user start
                    avahi-daemon after reading the warning, the service won't
                    get published so the application has to be restarted.
                    Therefore, the best option is to log an error message
                    and show a fatal error dialog.
                    
                    This ensures that the user starts the avahi-daemon and restarts
                    the application.
                */

                Log.ErrorMessage("Client failed to connect: {0}\n{1}",
                                    ex.Message,
                                    ex.StackTrace);
                
                iAvahiClient = null;
                
                Utility.ShowCannotPublishServiceDialog();
                /*Shutdown();*/
                
                return false;
                
            }

            if (iAvahiClient.State == ClientState.Running)
            {
                iFtpServer = new FTPServer(this, this, ShareRoot,ServerPort);
                iFtpServer.ShutdownEvent += OnFTPServerShutdown;
                PublishFTPServer();
            }
            
            return true;
        }
        
        private void StopDaemon()
        {
            if (!iDaemonRunning)
                return;

            Log.DebugMessage("Stopping daemon");

            StopZeroConf();
            iFtpServer.ShutdownServer(FTPServerShutdownReason.NormalTermination);
            iFtpServer = null;

            /* disposing the client causes a mono dump */            
            /*iAvahiClient.Dispose();*/
            iAvahiClient = null;
            
            iDaemonRunning = false;
            
        }

        private bool Start()
        {

            Log.DebugMessage(String.Concat(String.Concat(ShareRoot," exists: "),
                                                Directory.Exists(ShareRoot)));

            if (!Directory.Exists(ShareRoot))
                Directory.CreateDirectory(ShareRoot);
            
            /*make sure that the user has full access to the ShareRoot directory */
            Mono.Unix.Native.Syscall.chmod(ShareRoot, Mono.Unix.Native.FilePermissions.S_IRWXU);
            
            iDbusService.SetDaemon(this);
            iDbusService.StartPublishing();
            
            if (SharingEnabled)
                return StartDaemon();

            return false;
        }

        public void Shutdown()
        {

            Log.DebugMessage("Shutting down gshare");

            if (SharingEnabled)
                StopDaemon();

            iDbusService.StopPublishing();
            
            Application.Quit();

            /*
                TODO this is an ugly hack. the mainloop may not be running
                in all cases where Shutdown is invoked to force a System.Exit() 
                here
            */
            
            BreakMainLoop = true;
            
        }

        private static void HandleCommandLineArgs(string[] args)
        {
            if (Array.IndexOf(args,"--shutdown")  > -1)
            {
                ShutdownRunningDaemon();
                return;
            }
            else if (Array.IndexOf(args,"--version") > -1)
            {
                PrintVersion();
                return;
            }
            
            PrintUsage();
        }
        
        private static void PrintVersion()
        {
            Console.WriteLine(String.Format("GShare version {0:0.00}",Utility.GShareVersion));
        }
            
        private static void ShutdownRunningDaemon()
        {
            if (GShareService.ServiceRunning())
            {
                GShareService.FindDBusInstance().Shutdown();
                Console.WriteLine("Service terminated");
            }
            else
            {
                Console.WriteLine("No instance found running");
            }
        }
        
        private static void PrintUsage()
        {
            Console.WriteLine("Usage: gshared [command]");
            Console.WriteLine("gshared is the background service that shares your files.\nWhen executing gshared with no extra arguments, the gshared daemon is started");
            Console.WriteLine("--help\t\t\tPrint this help message");
            Console.WriteLine("--shutdown\t\tShutdown a running daemon");
            Console.WriteLine("--version\t\tPrint the GShare version");
        }
        
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init("gshared",ref args);
            Catalog.Init(GShare.Constants.I18N_DOMAIN,GShare.Constants.I18N_DIR);
            BusG.Init();
            
            if (args.Length > 0)
            {
                HandleCommandLineArgs(args);
                return;
            }
            
            if (GShareService.ServiceRunning())
            {
                
                /*only one service per user session */
                Console.WriteLine("An instance of gshared was found running");
                return;
            }
            
            GShared daemon = new GShared();

            try
            {
                daemon.Start();
                
                /*this is the continuation of the ugliest hack ever from above*/
                if (!BreakMainLoop)
                    Application.Run();

            }
            catch (Exception ex)
            {
                Log.ErrorMessage("Fatal error: {0}\n{1}", 
                                    ex.Message, 
                                    ex.StackTrace);
            
                return;
            }
            
            return;
        }
        
        /*this is shit code, check Shutdown() and Main() comments */
        private static bool BreakMainLoop = false;
        
    }
    

}
