/*

This file is part of GShare.

GShare is free software; you can redistribute it 
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

GShare  is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GShare; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The Initial Developer of the Original Code is Celso Pinto.
Portions created by Celso Pinto is Copyright (C) 2006. 

All Rights Reserved.

*/

using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Collections;
using System.Globalization;
using System.Text;

using Mono.Unix;

namespace GShare.FTP
{
    /*
        This enum is used to validate what ftp clients can do on the local
        filesystem. For now, if the account under which the daemon is running
        has these access permissions then the remote user will have them too.
    */
    [System.Flags]
    internal enum EFtpPermissions
    {
        CanRead = FileAccessPermissions.UserRead,
        CanWrite = FileAccessPermissions.UserWrite,
        CanExecute = FileAccessPermissions.UserExecute
    }
    
    [System.Flags]
    internal enum EFtpSessionState
    { 
        JustConnected   = 0,
        GotUsername     = 1 << 0,
        LoggedIn        = 1 << 1,
        InPasvMode      = 1 << 2,
        Quit            = 1 << 3
    }

    internal class DataConnectionHelper
    {
        private const int DATA_CONNECTION_TIMEOUT = 10*1000;
        private const int DATA_CONNECTION_TX_TIMEOUT = 60*1000; 
        private const int BUFFER_SIZE = 8192;
        
        public delegate void WriteResponseDelegate(string format,params object[] args);
        
        private WriteResponseDelegate WriteResponse;
        
        private string iOutput;
        private string iFilename;
        private long iStartingOffset = 0;
        
        private bool iAbortTransfer;
        private bool iTypeBinary = false;
        
        private TcpListener iDataConnectionServer = null;
        private Socket iDataConnection = null;

        public DataConnectionHelper(WriteResponseDelegate writeResponse)
        {
            WriteResponse = writeResponse;
        }

        public bool IsTransferring
        {
            get { return (iDataConnection != null); }
        }
        
        public bool BinaryTransfer
        {
            set { iTypeBinary = value; }
            get { return iTypeBinary; }
        }
        
        public long StartingOffset
        {
            set { iStartingOffset = value; }
            get { return iStartingOffset; }
        }
        
        public delegate void DataConnectionClosed();
        public event DataConnectionClosed DataConnectionClosedEvent;
        
        public void Close()
        {
            if (iDataConnection != null)
            {
                try { iDataConnection.Close(); } catch {}
            }
            
            if (iDataConnectionServer != null)
            {
                try { iDataConnectionServer.Stop(); } catch {}
            }
            
            iDataConnection = null;
            iDataConnectionServer = null;
            iAbortTransfer = false;            
        }
        
        private void CloseDataConnection()
        {
            Close();
            if (DataConnectionClosedEvent != null)
                DataConnectionClosedEvent();
                
        }
        
        private void StartDataConnection()
        {

            /* for the time being, we just poll the tcplistener every 
             * 500ms to check if there are any pending connections.
             * With Java, it would be a piece of cake because we could use the setSoTimeout.
             */
            int timer = 0;
            while (timer < DATA_CONNECTION_TIMEOUT && !iDataConnectionServer.Pending())
            {
                Thread.Sleep(500);
                timer += 500;
            }

            if (iDataConnectionServer.Pending())
                iDataConnection = iDataConnectionServer.AcceptSocket();
            else
                iDataConnection = null;

        }

        public int StartPassiveConnection()
        {
            iDataConnectionServer = new TcpListener(IPAddress.Any, 0);
            iDataConnectionServer.Start();

            return ((IPEndPoint)iDataConnectionServer.LocalEndpoint).Port;
        }
        
        public string ListOutput
        {
            set { iOutput = value; }
            get { return iOutput; }
        }
        
        public string FileName
        {
            set { iFilename = value; }
            get { return iFilename; }
            
        }
        
        public void AbortTransfer()
        {

            if (iDataConnection != null)
            {
                iAbortTransfer = true;
                iDataConnection.Shutdown(SocketShutdown.Both);                    
                WriteResponse("426 Transfer canceled");
                /* transfer methods already send 226 */
            }
            else
            {
                WriteResponse("226 Abort successful");
            }


        }
        
        public void SendLIST()
        {
            bool error = false;
            
            try
            {
                StartDataConnection();

                if (iDataConnection == null)
                {
                    WriteResponse("425 Can't open data connection.");
                    return;
                }

                using (iDataConnection)
                using (StreamWriter sw = new StreamWriter(new NetworkStream(iDataConnection)))
                {
                    sw.WriteLine(iOutput);
                    sw.Flush();
                }

            }
            catch(Exception ex)
            {

                if (!iAbortTransfer)
                {
                    Log.DebugMessage(ex.Message);
                    Log.DebugMessage(ex.StackTrace);
                }

                error = true;
            }
            
            CloseDataConnection();

            if (error && !iAbortTransfer)
                WriteResponse("551 Error sending LIST");
            else        
                WriteResponse("226 List command successful");
        }

        public void SendFile()
        {
            bool error = false;
            Log.DebugMessage("Sending file");

            try
            {

                StartDataConnection();

                if (iDataConnection == null)
                {
                    WriteResponse("425 Can't open data connection.");
                    return;
                }

                iDataConnection.SetSocketOption(SocketOptionLevel.Socket,
                                                SocketOptionName.SendTimeout,
                                                DATA_CONNECTION_TX_TIMEOUT);
                    
                using (iDataConnection)
                using (NetworkStream outs = new NetworkStream(iDataConnection))
                using (FileStream ins = new FileStream(iFilename,
                                                        FileMode.Open,
                                                        FileAccess.Read))
                {
                
                    Log.DebugMessage("Out is writeable: {0}", outs.CanWrite);
                    
                    byte[] block = new byte[BUFFER_SIZE];
                    
                    int readBytes = 0;
                    
                    ins.Seek(iStartingOffset,SeekOrigin.Begin);
                    
                    while((readBytes = ins.Read(block,0,BUFFER_SIZE)) != 0)
                    {                    
                        outs.Write(block,0,readBytes);
                        outs.Flush();   
                    }

                }

                
            }
            catch (IOException ex)
            {

                if (!iAbortTransfer)
                {
                    Log.DebugMessage(ex.Message);
                    Log.DebugMessage(ex.StackTrace);
                    
                    if (ex.InnerException != null)
                    {
                        Log.DebugMessage(ex.InnerException.Message);
                        Log.DebugMessage(ex.InnerException.StackTrace);
                    }
                }

                error = true;
                
            }
            
            CloseDataConnection();

            if (error && !iAbortTransfer)
                WriteResponse("451 Error sending file");
            else
                WriteResponse("226 File transferred successfully.");
            
            Log.DebugMessage("Send file finished");

        }
        
        public void ReceiveFile()
        {
            /*receive file*/
            bool error = false;
            try
            {

                StartDataConnection();

                if (iDataConnection == null)
                {
                    WriteResponse("425 Can't open data connection.");
                    return;
                }

                iDataConnection.SetSocketOption(SocketOptionLevel.Socket,
                                                SocketOptionName.ReceiveTimeout,
                                                DATA_CONNECTION_TX_TIMEOUT);
                    
                using (iDataConnection)
                using (NetworkStream ins = new NetworkStream(iDataConnection))
                using (FileStream outs = new FileStream(iFilename,
                                                        FileMode.OpenOrCreate,
                                                        FileAccess.Write))
                {

                    byte[] block = new byte[BUFFER_SIZE];
                    int readBytes = 0;
                    while((readBytes = ins.Read(block,0,BUFFER_SIZE)) != 0)
                    {
                        outs.Write(block,0,readBytes);
                        outs.Flush();
                    }
                    
                }

                
            }
            catch (Exception ex)
            {

                if (!iAbortTransfer)
                {
                    Log.DebugMessage(ex.Message);
                    Log.DebugMessage(ex.StackTrace);
                }

                error = true;    
            }

            CloseDataConnection();

            if (error && !iAbortTransfer)
                WriteResponse("451 Error receiving file");
            else
                WriteResponse("226 File transferred successfully.");
            
        }
      
    }

    internal class ClientFTPSession
    {
        /* consts */
        private const int CONNECTION_TIMEOUT = 10*60000;/* If nothing happens within ten minutes, close the session */
        
        private const string MDTM_DATE_FORMAT = "yyyyMMddhhmmss";
        private const string LS_DATE_FORMAT = "MMM dd HH:mm";
        
        private const string FILEPATH_SEPARATOR = "/";
        private const string FILEPATH_ROOT = "/";
        
        private static readonly IFormatProvider DATE_LOCALE = new CultureInfo("en-US");
        private readonly string FTP_USERNAME, FTP_GROUPNAME;

        /* event related stuff */
        public delegate void ConnectionClosedHandler(ClientFTPSession session);
        public event ConnectionClosedHandler ConnectionClosed;

        private delegate void ExecCommand(string[] args);

        /* members */
        private Hashtable iSupportedCommands;

        private IAuthenticationManager iAuthentication;
        private IPermissionManager iPermissions;
        
        private Socket iCtrlConnection;
        private StreamWriter iCtrlConnectionOut;
        private StreamReader iCtrlConnectionIn;
        private DataConnectionHelper iDataConnectionHelper;
        
        private string iRootDir;
        private string iCurrentDirectory = FILEPATH_ROOT;

        private EFtpSessionState iSessionState = EFtpSessionState.JustConnected;
        private string iUsername = "";
        private string iRenameFrom = null;
        
        private Timer iConnectionTimer;
        
        public ClientFTPSession(Socket client,
                                IAuthenticationManager auth,
                                IPermissionManager permissions,
                                string rootDir)
        {

            UnixUserInfo user = Mono.Unix.UnixUserInfo.GetRealUser();
           
            /* ftp == just covering my ass */
            FTP_USERNAME = (user.UserName != null ? user.UserName : "ftp");
            FTP_GROUPNAME = (user.GroupName != null ? user.GroupName : "ftp");
                
            iAuthentication = auth;
            iPermissions = permissions;
            iRootDir = rootDir;
            
            iCtrlConnection = client;
            iCtrlConnectionIn = new StreamReader(new NetworkStream(iCtrlConnection));            
            iCtrlConnectionOut = new StreamWriter(new NetworkStream(iCtrlConnection));
            iCtrlConnectionOut.NewLine = "\r\n";
            iCtrlConnectionOut.AutoFlush = true;

            iDataConnectionHelper = new DataConnectionHelper(WriteResponse);
            iDataConnectionHelper.DataConnectionClosedEvent += DataConnectionClosedHandler;
            
            iConnectionTimer = new Timer(CheckConnectionTimeout,null,Timeout.Infinite,Timeout.Infinite);            

            iSupportedCommands = new Hashtable(35, 0.1f);

            iSupportedCommands.Add("QUIT",new ExecCommand(HandleQUIT));
            iSupportedCommands.Add("LOGOUT", new ExecCommand(HandleQUIT));
            iSupportedCommands.Add("USER", new ExecCommand(HandleUSER));
            iSupportedCommands.Add("PASS", new ExecCommand(HandlePASS));

            iSupportedCommands.Add("PASV", new ExecCommand(HandlePASV));
            iSupportedCommands.Add("LIST", new ExecCommand(HandleLIST));
            iSupportedCommands.Add("LS", new ExecCommand(HandleLIST));

            iSupportedCommands.Add("SYST", new ExecCommand(HandleSYST));
            iSupportedCommands.Add("PWD", new ExecCommand(HandlePWD));
            iSupportedCommands.Add("TYPE", new ExecCommand(HandleTYPE));
            iSupportedCommands.Add("CWD", new ExecCommand(HandleCWD));
            iSupportedCommands.Add("CD", new ExecCommand(HandleCWD));
            iSupportedCommands.Add("CDUP", new ExecCommand(HandleCDUP));
            iSupportedCommands.Add("RETR",new ExecCommand(HandleRETR));

            iSupportedCommands.Add("RMD", new ExecCommand(HandleRMD));
            iSupportedCommands.Add("DELE", new ExecCommand(HandleDELE));
            iSupportedCommands.Add("MKD",new ExecCommand(HandleMKD));
            iSupportedCommands.Add("NOOP",new ExecCommand(HandleNOOP));

            iSupportedCommands.Add("RNFR", new ExecCommand(HandleRNFR));
            iSupportedCommands.Add("RNTO", new ExecCommand(HandleRNTO));

            iSupportedCommands.Add("STOR", new ExecCommand(HandleSTOR));
            
            iSupportedCommands.Add("SIZE",new ExecCommand(HandleSIZE));
            iSupportedCommands.Add("MDTM", new ExecCommand(HandleMDTM));
            
            iSupportedCommands.Add("ABOR", new ExecCommand(HandleABOR));
            
            iSupportedCommands.Add("REST", new ExecCommand(HandleREST));
            
            /*            
            iSupportedCommands.Add("PORT",new ExecCommand(HandlePORT));
            iSupportedCommands.Add("STRU",new ExecCommand(HandleSTRU));
            iSupportedCommands.Add("MODE",new ExecCommand(HandleMODE));
            iSupportedCommands.Add("NLST",new ExecCommand(HandleNLST));
            iSupportedCommands.Add("STAT",new ExecCommand(HandleSTAT));
            */
            
        }

        public Socket ClientSocket
        {
            get { return iCtrlConnection; }
        }

        private string GetAbsolutePath(string path)
        {

            string fullpath = Path.GetFullPath(String.Concat(iRootDir,path));

            if (fullpath.StartsWith(iRootDir))
            {
                return fullpath;
            }
            else
            {
                for(int i=0;i<fullpath.Length && i<iRootDir.Length;i++)
                {
                    if (fullpath[i] != iRootDir[i])
                    {
                        string subs = fullpath.Substring(0,i);
                        int lastIdx = subs.LastIndexOf(FILEPATH_SEPARATOR);
                        return fullpath.Replace(fullpath.Substring(0,lastIdx),iRootDir);
                    }
                }
                
                /*worst-case scenario */
                return String.Concat(iRootDir,fullpath);
            }

        }

        private string EscapeDirname(string name)
        {
            return name.Replace("\"", "\"\"");
        }

        private void CheckConnectionTimeout(object data)
        {
            /* if timer triggered without anything down the communication path*/
            /* and no transfers current in place*/
            /* shutdown the socket */
            if (!iDataConnectionHelper.IsTransferring)
                iCtrlConnection.Shutdown(SocketShutdown.Both);

        }
        
        private bool HasPermissions(FileAccessPermissions filePerms,EFtpPermissions ftpPerms)
        {
            return ( ((int)filePerms & (int)ftpPerms) != 0 ); 
        }
        
        public void DataConnectionClosedHandler()
        {
            iSessionState = EFtpSessionState.LoggedIn;
            /* reset connection timer */
            iConnectionTimer.Change(CONNECTION_TIMEOUT ,Timeout.Infinite);        
            iTransferOffset = 0;
        }


        public void StartClientSession()
        {

            WriteResponse("220-GShare FTP Server");
            WriteResponse("220-What files do you want today? ");
            WriteResponse("220 ");
            
            string[] command;
            string temp;
            try
            {

                while (iSessionState != EFtpSessionState.Quit)
                {

                    temp = iCtrlConnectionIn.ReadLine();

                    if (temp == null) /* connection closed by client */
                        break;

                    temp = temp.Trim();

                    Log.DebugMessage(temp);

                    if (temp.Length == 0)
                        continue;

                    command = temp.Split(' ');
                    if (!iSupportedCommands.ContainsKey(command[0].ToUpper()))
                    {
                        WriteBadCommandResponse();
                        continue;
                    }

                    ((ExecCommand)iSupportedCommands[command[0].ToUpper()])(command);

                    /* reset connection timer */
                    iConnectionTimer.Change(CONNECTION_TIMEOUT ,Timeout.Infinite);
                    
                }
            }
            catch (Exception ex)
            {
                /*Console.WriteLine(ex.StackTrace);*/
                /* this probably occurs because of a socket receive timeout */
            }

            if (ConnectionClosed != null)
                ConnectionClosed(this);

        }

        public void StopClientSession()
        {
        
            iDataConnectionHelper.Close();

            try { iCtrlConnectionOut.Close(); } catch { /*NOFAIL */ }
            try { iCtrlConnectionIn.Close(); } catch { /*NOFAIL */ }
            try { iCtrlConnection.Close(); } catch { /*NOFAIL */ }
        }

        private bool AssertNrOfArguments(string[] args, int minimumNrOfArgs)
        {
            if (args.Length < minimumNrOfArgs)
            {
                WriteResponse("501 Missing arguments");
                return false;
            }
            
            return true;
        }
        
        private string ConcatArgs(string[] args,int startIndex)
        {
            string res = "";
            
            for(int i=startIndex;i<args.Length;i++)
                res = String.Concat(res,String.Concat(args[i]," "));
                
            return res.Trim();
        }
        
        private void WriteResponse(string format, params object[] args)
        {

            Log.DebugMessage(format, args);

            lock(iCtrlConnectionOut)
            {
                iCtrlConnectionOut.WriteLine(format, args);
            }

        }

        private void WriteBadCommandResponse()
        {
            switch (iSessionState)
            { 
                case EFtpSessionState.JustConnected:
                    WriteResponse("503 Bad sequence of command, USER required.");
                    break;
                case EFtpSessionState.GotUsername:
                    WriteResponse("503 Bad sequence of command, PASS required.");
                    break;
                default:
                    WriteResponse("500 Syntax error, command unrecognized.");
                    break;
            }
        }

        private string PathConcat(string path1,string path2)
        {
            if (!path1.EndsWith(FILEPATH_SEPARATOR) && !path2.StartsWith(FILEPATH_SEPARATOR))
                path2 = String.Concat(FILEPATH_SEPARATOR,path2);
                
            return String.Concat(path1,path2);
        }

        /*
            References:
          
            http://www.ietf.org/rfc/rfc0959.txt
            http://www.the-eggman.com/seminars/ftp_error_codes.html

            http://www.networksorcery.com/enp/protocol/ftp.htm
            http://www.jtpfxp.net/rawcmd.htm
             
         */

        private void HandleUSER(string[] args)
        {
            if (iSessionState == EFtpSessionState.JustConnected)
            {
                if (!AssertNrOfArguments(args,2)) return;
                
                iUsername = ConcatArgs(args,1);
                WriteResponse("331 User name okay, need password.");
                iSessionState = EFtpSessionState.GotUsername;

            }
            else
                WriteBadCommandResponse();
        }

        private void HandlePASS(string[] args)
        {
            if (iSessionState == EFtpSessionState.GotUsername)
            {
                if (!AssertNrOfArguments(args,2)) return;
                
                if (iAuthentication.AuthenticateUser(iUsername, ConcatArgs(args,1)))
                {
                    WriteResponse("230 User logged in, proceed.");
                    iSessionState = EFtpSessionState.LoggedIn;
                }
                else 
                {
                    iSessionState = EFtpSessionState.JustConnected;
                    iUsername = "";
                    WriteResponse("530 Not logged in.");
                }

            }
            else
                WriteBadCommandResponse();

        }

        private void HandleRNTO(string[] args)
        {
            /*Depends on RNFR being previously sent */
            /*
                Syntax: RNTO to-filename
                Used when renaming a file. After sending an RNFR command to specify the 
                file to rename, send this command to specify the new name for the file.
            */
            if (!AssertNrOfArguments(args,2)) return;

            if (iRenameFrom == null)
            {
                WriteResponse("503 Send RNFR first");
                return;
            }
            
            string toFilename = GetAbsolutePath(PathConcat(iCurrentDirectory,ConcatArgs(args,1)));
            
            FileAttributes mode = File.GetAttributes(iRenameFrom);
            UnixFileSystemInfo infoFrom,infoTo;
            
            if (mode == FileAttributes.Directory)
            {
                infoFrom = new UnixDirectoryInfo(iRenameFrom);
                infoTo = new UnixDirectoryInfo(toFilename);
            }
            else
            {
                infoFrom = new UnixFileInfo(iRenameFrom);
                infoTo = new UnixFileInfo(toFilename);
            }

            if (infoTo.Exists)
            {
                WriteResponse("553 Already exists");
                return;
            }
            
            if (!(  iPermissions.AllowWrite &&
                    HasPermissions( infoFrom.FileAccessPermissions,
                                    EFtpPermissions.CanWrite) ))
            {
                WriteResponse("550 Not enough permissions");
                return;
            }

            try
            {

                if (mode == FileAttributes.Directory)
                    Directory.Move(iRenameFrom,toFilename);
                else
                    File.Move(iRenameFrom,toFilename);

            }
            catch(Exception ex)
            {
                WriteResponse("550 Rename failed");
                return;
            }

            WriteResponse("250 Rename successful");
            iRenameFrom = null;

        }

        private void HandleRNFR(string[] args)
        {
            /*
             
                Syntax: RNFR from-filename
                Used when renaming a file. Use this command to specify the file 
                to be renamed; follow it with an RNTO command to specify the new 
                name for the file.
             
             */
            
            if (!AssertNrOfArguments(args,2)) return;

            string path = GetAbsolutePath(PathConcat(iCurrentDirectory,ConcatArgs(args,1)));
            
            if (!Directory.Exists(path) && !File.Exists(path))
            {
                WriteResponse("550 Not found.");
                return;
            }
            
            iRenameFrom = path;
            WriteResponse("350 Now send RNTO");
            
        }

        private void HandleRMD(string[] args)
        {
            /*
             
                Syntax: RMD remote-directory
                Deletes the named directory on the remote host.             
             
             */
            if (!AssertNrOfArguments(args,2)) return;

            string dir = ConcatArgs(args,1);

            DirectoryInfo dirInfo = new DirectoryInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,dir)));
            if (!dirInfo.Exists)
            {
                WriteResponse("550 No such directory: {0}",dir);
                return;
            }
            
            if (!iPermissions.AllowDelete)
            {
                WriteResponse("550 Not enough permissions");
                return;
            }


            Log.DebugMessage("Removing {0}",dirInfo.FullName);

   
            dirInfo.Delete();
            WriteResponse("250 Directory deleted.");
        }

        private void HandleMDTM(string[] args)
        {
            /*
             
                Syntax: MDTM remote-filename
                Returns the last-modified time of the given file on the 
                remote host in the format "YYYYMMDDhhmmss": YYYY is the four-digit year, 
                MM is the month from 01 to 12, DD is the day of the month from 01 to 31, 
                hh is the hour from 00 to 23, mm is the minute from 00 to 59, and ss is 
                the second from 00 to 59.
             
             */

            if (!AssertNrOfArguments(args,2)) return;

            string path = GetAbsolutePath(PathConcat(iCurrentDirectory,ConcatArgs(args,1)));
            if (!File.Exists(path) && !Directory.Exists(path))
            {
                WriteResponse("550 Not found");
                return;
            }
            
            FileAttributes mode = File.GetAttributes(path);
            String timestamp;
            
            if (mode == FileAttributes.Directory)
            {
                timestamp = Directory.GetLastWriteTime(path).ToString(MDTM_DATE_FORMAT,DATE_LOCALE);                
            }
            else
            {
                timestamp = File.GetLastWriteTime(path).ToString(MDTM_DATE_FORMAT,DATE_LOCALE);
            }
            
            WriteResponse(String.Concat("213 ",timestamp));
            
        }
        
        private void HandleDELE(string[] args)
        {
            /*
             
                Syntax: DELE remote-filename
                Deletes the given file on the remote host.                
             
             */

            if (!AssertNrOfArguments(args,2)) return;

            string file = ConcatArgs(args,1);

            UnixFileInfo fileInfo = new UnixFileInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,file)));
            if (!fileInfo.Exists)
            {
                WriteResponse("550 No such file: {0}",file);
                return;
            }

            if (!(  iPermissions.AllowDelete && 
                    HasPermissions( fileInfo.FileAccessPermissions,
                                    EFtpPermissions.CanWrite) ))
            {
                WriteResponse("550 Not enough permissions");
                return;
            }
            
            Log.DebugMessage("Deleting {0}",fileInfo.FullName);

            fileInfo.Delete();
            WriteResponse("250 File deleted.");

        }

        private void HandleSTAT(string[] args)
        {
            /*
             
                Syntax: STAT [remote-filespec]
                If invoked without parameters, returns general status information 
                about the FTP server process. If a parameter is given, acts like 
                the LIST command, except that data is sent over the control connection 
                (no PORT or PASV command is required).
             
             */

        }

        private void HandleSTOR(string[] args)
        {
            /*
             
                Syntax: STOR remote-filename
                Begins transmission of a file to the remote site. Must be preceded by 
                either a PORT command or a PASV command so the server knows where 
                to accept data from.
             
             */
             
            if (!AssertNrOfArguments(args,2)) return;

            if (iSessionState != EFtpSessionState.InPasvMode || iDataConnectionHelper.IsTransferring)
            {
                WriteResponse("425 Can't open data connection.");
                return;
            }
            
            string file = ConcatArgs(args,1);

            UnixFileInfo fileInfo = new UnixFileInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,file)));
            if (!fileInfo.Directory.Exists)            
            {
                WriteResponse("550 Parent directory not found");
                return;
            }

            if ((!iPermissions.AllowWrite) ||
                    (fileInfo.Exists && 
                    !HasPermissions(fileInfo.FileAccessPermissions,
                                    EFtpPermissions.CanWrite) ))
            {
                WriteResponse("550 Not enough permissions");
                return;
            }

            WriteResponse("150 Opening data connection");

            Log.DebugMessage("Receiving {0}",fileInfo.FullName);

            iDataConnectionHelper.StartingOffset = iTransferOffset;
            iDataConnectionHelper.FileName = fileInfo.FullName;
            Utility.InvokeLater(iDataConnectionHelper.ReceiveFile);            
            
        }

        private void HandleSIZE(string[] args)
        {
            /*

                Syntax: SIZE remote-filename
                Returns the size of the remote file as a decimal number.
            */
            if (!AssertNrOfArguments(args,2)) return;

            string file = ConcatArgs(args,1);

            FileInfo fileInfo = new FileInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,file)));
            if (!fileInfo.Exists)
            {
                WriteResponse("550 File not found");
                return;
            }
            
            WriteResponse("213 " + fileInfo.Length);
        }

        private void HandleNOOP(string[] args)
        {
            /*
             
                Syntax: NOOP
                Does nothing except return a response.             
             
             */
             WriteResponse("200 Command okay");
        }

        private void HandleSYST(string[] args)
        {

            /*
             
                Syntax: SYST
                Returns a word identifying the system, the word "Type:", 
                and the default transfer type (as would be set by the TYPE command). 
                For example: UNIX Type: L8             
             
             */
            
            /*This is hardcoded because Firefox doesn't like the output of OSVersion w/ Mono on Linux */
            switch((int)Environment.OSVersion.Platform)
            {
                case 4:/*mono 2.0*/
                case 128:/*mono 1.1*/
                    WriteResponse("215 UNIX");
                    break;
                default:
                    WriteResponse("215 Win32");
                    break;
            }
            
        }

        private void HandleNLST(string[] args)
        {
            /*Depends on PASV being sent */

            /*
             
                Syntax: NLST [remote-directory]
                Returns a list of filenames in the given directory (defaulting to 
                the current directory), with no other information. Must be preceded 
                by a PORT or PASV command.
             
             */
        }

        private string GetFileType(UnixFileSystemInfo info)
        {

            if (info is UnixDirectoryInfo)
                return "d";

            if (info is UnixSymbolicLinkInfo &&
                ((UnixSymbolicLinkInfo)info).Contents is UnixDirectoryInfo)
                return "d";

            return "-";

        }

        private void HandleLIST(string[] args)
        {
            /*Depends on PASV being sent */
            /*
             
                Syntax: LIST [remote-filespec]
                If remote-filespec refers to a file, sends information about that 
                file. If remote-filespec refers to a directory, sends information 
                about each file in that directory. remote-filespec defaults to the 
                current directory. This command must be preceded by a PORT or PASV command.
             
                NOTE: my implementation *only supports directories*!!!! 
             
             */

            if (iSessionState != EFtpSessionState.InPasvMode || iDataConnectionHelper.IsTransferring)
            {
                WriteResponse("425 Can't open data connection.");
                return;
            }

            string dir = "";            
            if (args.Length > 1 && args[1].Trim().StartsWith("-")) /* doesn't support any arguments, yet... */
                dir = ConcatArgs(args,2);
            else if (args.Length > 1)
                dir = ConcatArgs(args,1);
            
            UnixDirectoryInfo dirInfo = new UnixDirectoryInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,dir)));
            if (!dirInfo.Exists)
            {
                WriteResponse("550 No such directory: {0}",dir);
                return;
            }

            try
            {

                const string format = "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}  1 {10} {11} {12} {13} {14}\r\n";

                StringBuilder data = new StringBuilder(1024);
                
                data.AppendFormat(  format, 
                                    "d", 
                                    "r","w","x",
                                    "r","w","x",
                                    "r","w","x",
                                    FTP_USERNAME,
                                    FTP_GROUPNAME,
                                    0, 
                                    dirInfo.LastWriteTime.ToString(LS_DATE_FORMAT, DATE_LOCALE), 
                                    ".");
                
                UnixDirectoryInfo parentInfo = dirInfo.Parent;

                string parentDateTime;
                if (parentInfo == null)
                    parentDateTime = dirInfo.LastWriteTime.ToString(LS_DATE_FORMAT, DATE_LOCALE);
                else
                    parentDateTime = parentInfo.LastWriteTime.ToString(LS_DATE_FORMAT, DATE_LOCALE);
                    
                data.AppendFormat(  format, 
                                    "d", 
                                    "r","w","x",
                                    "r","w","x",
                                    "r","w","x",
                                    FTP_USERNAME,
                                    FTP_GROUPNAME,
                                    0, 
                                    parentDateTime, 
                                    "..");
                
                string writePermissions = (iPermissions.AllowWrite ? "w" : "-");
                long fileLength;

                foreach (UnixFileSystemInfo info in dirInfo.GetFileSystemEntries())
                {
                
                    if (info.FileType != FileTypes.Directory &&
                        info.FileType != FileTypes.RegularFile &&
                        info.FileType != FileTypes.SymbolicLink)
                    {
                        continue;
                    }
                    
                    /* for now ugo=rwx has to be enough!!! */
                    fileLength = info.Length;

                    if (info is UnixSymbolicLinkInfo)
                    {

                        if (!((UnixSymbolicLinkInfo)info).HasContents)
                            continue; /*skip bad symlinks */

                        /*
                            This triggers Mono bug #78580
                            http://bugzilla.ximian.com/show_bug.cgi?id=78580
                            
                            TODO clean this when bugfix is released
                        */
                        fileLength = ((UnixSymbolicLinkInfo)info).Contents.Length;

                    }
                    
                    data.AppendFormat(  format,
                                        GetFileType(info),
                                        "r", writePermissions, "x", /*u*/
                                        "r", writePermissions, "x", /*g*/
                                        "r", writePermissions, "x", /*o*/
                                        FTP_USERNAME,
                                        FTP_GROUPNAME,
                                        fileLength,
                                        info.LastWriteTime.ToString(LS_DATE_FORMAT, DATE_LOCALE),
                                        info.Name);
                }

                WriteResponse("150 Opening data connection for LIST {0}",dir);

                iDataConnectionHelper.ListOutput = data.ToString();
                Utility.InvokeLater(iDataConnectionHelper.SendLIST);

            }
            catch(Exception ex)
            {
                Log.DebugMessage(ex.Message);
                Log.DebugMessage(ex.StackTrace);
                WriteResponse("551 Error sending LIST");
            }

        }

        private void HandlePWD(string[] args)
        {
            /*
             
                Syntax: PWD
                Returns the name of the current directory on the remote host.
             
             */
            WriteResponse("257 \"{0}\" is current directory.",EscapeDirname(iCurrentDirectory));
        }

        private void HandleCWD(string[] args)
        {

            /*
             
                Syntax: CWD remote-directory
                Makes the given directory be the current directory on the remote host.
             
             */

            if (args.Length == 1) /*as in (args == 'CWD') */
            {
                iCurrentDirectory = FILEPATH_ROOT;
            }
            else 
            {

                string dir = ConcatArgs(args,1);
                
                if (!dir.StartsWith(FILEPATH_ROOT))
                    dir = PathConcat(iCurrentDirectory,dir);

                UnixDirectoryInfo dirInfo = new UnixDirectoryInfo(GetAbsolutePath(dir));

                if (dirInfo.Exists && 
                    HasPermissions( dirInfo.FileAccessPermissions,
                                    EFtpPermissions.CanExecute ))
                {
                    iCurrentDirectory = dir;
                }
                else
                {
                    WriteResponse("500 The directory name is invalid: {0}", dir);
                    return;
                }
            }

            WriteResponse("250 CWD Command successful.");

        }

        private void HandleMKD(string[] args)
        {
            /*
             
                Syntax: MKD remote-directory
                Creates the named directory on the remote host.
             
             */
            if (!AssertNrOfArguments(args,2)) return;
            
            string directory = ConcatArgs(args,1);
            UnixDirectoryInfo dirInfo = new UnixDirectoryInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,directory)));
            
            if (!(  iPermissions.AllowWrite &&
                    HasPermissions( dirInfo.Parent.FileAccessPermissions,
                                    EFtpPermissions.CanWrite) ))
            {
                WriteResponse("550 Not enough permissions");
                return;
            }
            
            if (dirInfo.Exists)
            {
                WriteResponse("553 Directory already exists");
                return;
            }
            
            
            dirInfo.Create();
            WriteResponse("257 \"{0}\"Directory created.",directory);

        }

        private void HandleREST(string[] args)
        {
            /*Depends on PASV being sent */

            /*
                Syntax: REST offset
                Sets the point at which a file transfer should start; useful for
                resuming interrupted transfers. For nonstructured files, 
                this is simply a decimal number. This command must immediately 
                precede a data transfer command (RETR or STOR only);
                
                i.e. it must come after any PORT or PASV command.  
             */
            
            if (!AssertNrOfArguments(args,2)) return;

            if (iSessionState != EFtpSessionState.InPasvMode || iDataConnectionHelper.IsTransferring)
            {
                WriteResponse("503 Send PASV first");
                return;
            }

            try
            {
                iTransferOffset = Int64.Parse(args[1]);
            }
            catch(FormatException ex)
            {
                WriteResponse("501 Syntax error in parameters or arguments.");
            }
            catch(OverflowException ex)
            {
                WriteResponse("501 Syntax error in parameters or arguments.");
            }
            
            WriteResponse("350 Pending further information.");

        }

        private long iTransferOffset = 0;
        
        private void HandleRETR(string[] args)
        {
            /*Depends on PASV being sent */

            /*
             
                Syntax: RETR remote-filename
                Begins transmission of a file from the remote host. 
                Must be preceded by either a PORT command or a PASV command to indicate 
                where the server should send data.             
             
             */
            
            if (!AssertNrOfArguments(args,2)) return;

            if (iSessionState != EFtpSessionState.InPasvMode || iDataConnectionHelper.IsTransferring)
            {
                WriteResponse("425 Can't open data connection.");
                return;
            }
                
            string file = ConcatArgs(args,1);
            
            UnixFileInfo fileInfo = new UnixFileInfo(GetAbsolutePath(PathConcat(iCurrentDirectory,file)));
            
            if (!fileInfo.Exists)
            {
                WriteResponse("550 No such file: {0}",file);
                return;
            }

            if (!HasPermissions(fileInfo.FileAccessPermissions,
                                EFtpPermissions.CanRead))
            {
                WriteResponse("550 Not enough permissions");
                return;
            }
            
            WriteResponse("150 Opening data connection for RETR {0}",file);
            
            Log.DebugMessage("Sending {0}",fileInfo.FullName);

            iDataConnectionHelper.FileName = fileInfo.FullName;
            iDataConnectionHelper.StartingOffset = iTransferOffset;
            
            Utility.InvokeLater(iDataConnectionHelper.SendFile);            

        }

        private void HandleMODE(string[] args)
        {
            /*
             
                Syntax: MODE mode-character

                Sets the transfer mode to one of:

                    * S - Stream
                    * B - Block
                    * C - Compressed

                The default mode is Stream. 
             
             */

        }

        private void HandleSTRU(string[] args)
        {
            /*
             
                Syntax: STRU structure-character

                Sets the file structure for transfer to one of:

                    * F - File (no structure)
                    * R - Record structure
                    * P - Page structure

                The default structure is File. 
             
                NOTE: and I don't support anything of this
             */
        }

        private void HandleTYPE(string[] args)
        {

            /*
             
                Syntax: TYPE type-character [second-type-character]

                Sets the type of file to be transferred. type-character can be any of:

                    * A - ASCII text
                    * E - EBCDIC text
                    * I - image (binary data)
                    * L - local format

                For A and E, the second-type-character specifies how the text should be 
                interpreted. It can be:

                    * N - Non-print (not destined for printing). This is the default if 
                            second-type-character is omitted.
             
                    * T - Telnet format control (<CR>, <FF>, etc.)
                    * C - ASA Carriage Control

                For L, the second-type-character specifies the number of bits per byte on the 
                local system, and may not be omitted. 
             
             */
            
            if (!AssertNrOfArguments(args,2)) return;
            
            if (args.Length != 2)
            {
                WriteResponse("504 Command not implemented for that parameter.");
                return;
            }

            string arg = args[1].ToUpper();
            if (!(arg.Equals("I")/* || arg.Equals("A")*/))
            {
                WriteResponse("504 Command not implemented for that parameter.");
                return;
            }

            iDataConnectionHelper.BinaryTransfer = arg.Equals("I");

            WriteResponse("200 Command ok");
        }

        private void HandlePASV(string[] args)
        {
            /*
                When the client sends a new PASV command, close any existing connections
             */
            iDataConnectionHelper.Close();

            try
            {
                int port = iDataConnectionHelper.StartPassiveConnection();
                
                IPAddress localAddress = ((IPEndPoint)iCtrlConnection.LocalEndPoint).Address;
                WriteResponse("227 Entering Passive Mode ({0},{1},{2})",
                                localAddress.ToString().Replace('.', ','),
                                ((int)port / 256), port % 256);

                iSessionState = EFtpSessionState.InPasvMode;
            }
            catch
            {
                iDataConnectionHelper.Close();
                WriteResponse("425 Can't open data port");
            }
        }

        private void HandleCDUP(string[] args)
        {
            /*
             
                Syntax: CDUP
                Makes the parent of the current directory be the current directory.
             
             */
            HandleCWD(new string[] {"cwd",".."});
        }

        private void HandleABOR(string[] args)
        {

            /*
                Syntax: ABOR
                Aborts a file transfer currently in progress.             
             */
            Log.DebugMessage("GOT ABOR");
            iDataConnectionHelper.AbortTransfer();
            
        }

        private void HandleQUIT(string[] args)
        {
            WriteResponse("221 Logged out.");
            iSessionState = EFtpSessionState.Quit;
        }

    }

    public enum FTPServerShutdownReason
    { 
        Error,
        NormalTermination
    }

    public delegate void FTPServerShutdownHandler(FTPServerShutdownReason reason);

    public class FTPServer : System.Net.Sockets.TcpListener
    {

        public event FTPServerShutdownHandler ShutdownEvent;
    
        private Hashtable iConnectedClients = null;

        private bool iRunning = true;
        private bool iShuttingDown = false;
        
        private IAuthenticationManager iAuthentication;
        private IPermissionManager iPermissions;
        
        private string iRootDir;

        public FTPServer(   IAuthenticationManager auth,
                            IPermissionManager permissions,
                            string rootDir,
                            int serverPort)
            : base(IPAddress.Any,serverPort)
        {
            iConnectedClients = new Hashtable();
            iAuthentication = auth;
            iPermissions = permissions;
            
            iRootDir = rootDir;
        }
        
        public int ServerPort
        {
            get { return ((IPEndPoint)LocalEndpoint).Port; }
        }
        
        public void StartServer()
        {
            iRunning = true;
            Start();
            Utility.InvokeLater(AcceptClients);
            
        }
        
        public void ShutdownServer(FTPServerShutdownReason reason)
        {
            if (!iRunning)
                return;

            iRunning = false;
            iShuttingDown = true;
            
            try
            {
                Stop();
            }
            catch { /*NOFAIL*/ }


            foreach (ClientFTPSession session in iConnectedClients.Values)
            {
                session.StopClientSession();
            }

            iConnectedClients.Clear();

            if (ShutdownEvent != null)
                ShutdownEvent(reason);

        }

        private void OnConnectionClosed(ClientFTPSession session)
        {
            session.StopClientSession();
            
            lock(iConnectedClients)
            {
                iConnectedClients.Remove(session.ClientSocket);
            }
            
            Log.DebugMessage("Client disconnected, nr of clients: {0}",
                                    iConnectedClients.Count.ToString());

        }

        private void AcceptClients()
        {
            ClientFTPSession session;
            Socket client = null;

            try
            {

                while (iRunning)
                {
                    /*
                        TODO only accept a certain number of clients. 
                        Make it a gconf setting. 
                        UPDATE: this is a really bad idea, given the
                                problems in GNOME-VFS FTP client
                     */
                    client = AcceptSocket();

                    session = new ClientFTPSession(client,iAuthentication,iPermissions,iRootDir);
                    client = null;
                    
                    lock(iConnectedClients)
                    {
                        iConnectedClients.Add(session.ClientSocket, session);
                        Log.DebugMessage("Client connected, nr of clients: {0}",
                                                iConnectedClients.Count.ToString());
                    }


                    session.ConnectionClosed += OnConnectionClosed;
                    Utility.InvokeLater(session.StartClientSession);
                }

            }
            catch (Exception ex)
            {
                if (client != null)
                    client.Close();

                if (!iShuttingDown)
                {

                    Log.DebugMessage(ex.ToString());

                    Log.ErrorMessage (  "Exception caught on ftpserver: {0}. The FTP Server will " + 
                                                "now shutdown.\n{1}", 
                                            ex.Message, 
                                            ex.StackTrace);

                    ShutdownServer(FTPServerShutdownReason.Error);
                    
                }
                
            }
            
        }
        
    }
}
