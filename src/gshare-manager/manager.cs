/*

This file is part of GShare.

GShare is free software; you can redistribute it 
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

GShare  is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GShare; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The Initial Developer of the Original Code is Celso Pinto.
Portions created by Celso Pinto is Copyright (C) 2006. 

All Rights Reserved.

*/

using System;
using System.Text.RegularExpressions;

using Glade;
using Gtk;
using Mono.Unix;

using GShare;

namespace GShare.GUI
{
    public class GShareManager
    {
        [Glade.Widget] private Window iMainWindow;

        [Glade.Widget] private CheckButton iAllowAnonCheckBox;
        [Glade.Widget] private Table iLoginTable;
        [Glade.Widget] private Entry iUsernameEntry;
        [Glade.Widget] private Entry iPasswordEntry;
        [Glade.Widget] private CheckButton iAllowChangesCheckBox;
        [Glade.Widget] private VBox iMiddleVBox;
        [Glade.Widget] private CheckButton iEnableServiceCheckBox;
        [Glade.Widget] private Entry iShareNameEntry;
        [Glade.Widget] private SpinButton iServerPortSpinner;

        private IGShareService iDBusService;
        private Gnome.Program iProgram;

        public GShareManager()
        {
        }

        public void OnEnableServiceCheckBoxToggled(object o, EventArgs args)
        {
            SetUIState();   
        }
        
        public void OnMainWindowDelete(object o, DeleteEventArgs args)
        {
            Quit(false);
        }

        public void OnAllowAnonCheckBoxToggled(object o, EventArgs args)
        {
            SetLoginUIState();
        }

        public void OnCloseButtonClicked(object o, EventArgs args)
        {
            Quit(true);
        }

        public void OnHelpButtonClicked(object o, EventArgs args)
        {

            Gnome.Help.DisplayDesktopOnScreen(  iProgram,
                                                Constants.GSHARE_HELP_DIR,
                                                "gshare.xml",
                                                "gshare-getting-started",
                                                iMainWindow.Screen);
                                                
        }
        
        private void Quit(bool saveChanges)
        {
            
            if (saveChanges)
            {
                if (!ValidSettings())
                    return;

                iDBusService.SetAllowAnonymousLogin(iAllowAnonCheckBox.Active);
                iDBusService.SetLogin(iUsernameEntry.Text, iPasswordEntry.Text);

                iDBusService.SetPermissions(iAllowChangesCheckBox.Active, 
                                            iAllowChangesCheckBox.Active);
       
                iDBusService.SetShareName(iShareNameEntry.Text);
                iDBusService.SetServerPort((int)iServerPortSpinner.Value);
                bool wasStarted = iDBusService.SetSharingEnabled(iEnableServiceCheckBox.Active);
                if (!wasStarted)
                {
                    ResetUIState();
                    return;
                }
                
            }

            if (!iEnableServiceCheckBox.Active)
                iDBusService.Shutdown();
                   
            Application.Quit();
            
        }
        
        private bool ValidSettings()
        {
            if (!iAllowAnonCheckBox.Active)
            { 

                String 
                    username = iUsernameEntry.Text.Trim(), 
                    password = iPasswordEntry.Text.Trim();

                if (username.Length == 0)
                {
                    using(Gtk.MessageDialog warningDialog = 
                            new Gtk.MessageDialog(  iMainWindow,
                                                    DialogFlags.Modal,
                                                    MessageType.Error,
                                                    ButtonsType.Close,
                                                    Catalog.GetString("Username cannot be empty")))
                    {
                        warningDialog.WindowPosition = WindowPosition.CenterOnParent;
                        warningDialog.Run();
                        warningDialog.Destroy();
                    }
                    
                    iUsernameEntry.GrabFocus();
                    return false;
                }

                if (Regex.IsMatch(username, @"[^\w]"))
                {
                    using(Gtk.MessageDialog warningDialog = 
                            new Gtk.MessageDialog(  iMainWindow,
                                                    DialogFlags.Modal,
                                                    MessageType.Error,
                                                    ButtonsType.Close,
                                                    Catalog.GetString("Username contains invalid characters.\nValid characters are a-z, A-Z, 0-9 and _.")))
                    {
                        warningDialog.WindowPosition = WindowPosition.CenterOnParent;
                        warningDialog.Run();
                        warningDialog.Destroy();
                    }

                    iUsernameEntry.GrabFocus();
                    iUsernameEntry.SelectRegion(0, -1);
                    return false;
                }

                if (password.Length == 0)
                {
                    using(Gtk.MessageDialog warningDialog = 
                            new Gtk.MessageDialog(  iMainWindow,
                                                    DialogFlags.Modal,
                                                    MessageType.Error,
                                                    ButtonsType.Close,
                                                    Catalog.GetString("Password cannot be empty")))
                    {
                        warningDialog.WindowPosition = WindowPosition.CenterOnParent;                          
                        warningDialog.Run();
                        warningDialog.Destroy();
                    }

                    iPasswordEntry.GrabFocus();
                    return false;
                }

                /*the password can have whatever characters the user sees fit */

            }

            /*TODO test share name */
            /*TODO test server port, try to connect, if unavailable show message */
            return true;

        }

        private void ResetUIState()
        {
            iEnableServiceCheckBox.Active = iDBusService.GetSharingEnabled();

            iAllowAnonCheckBox.Active = iDBusService.GetAllowAnonymousLogin();
            iPasswordEntry.Text = iDBusService.GetPassword();
            iUsernameEntry.Text = iDBusService.GetUsername();

            iAllowChangesCheckBox.Active = (iDBusService.GetAllowDelete() || 
                                            iDBusService.GetAllowWrite());
                                            

            iShareNameEntry.Text = iDBusService.GetShareName();
            iServerPortSpinner.Value = iDBusService.GetServerPort();

            SetUIState();
        }

        private void SetUIState()
        {
            iMiddleVBox.Sensitive = iEnableServiceCheckBox.Active;
            SetLoginUIState();
        }
        
        private void SetLoginUIState()
        {
            iLoginTable.Sensitive = !iAllowAnonCheckBox.Active;
        }

        public IGShareService GetDBusService()
        {
            IGShareService dbusService = GShareService.FindDBusInstance();

            if (dbusService == null)
            {
                using (Gtk.MessageDialog errorDialog =
                        new Gtk.MessageDialog(  null, 
                                                DialogFlags.Modal, 
                                                MessageType.Error, 
                                                ButtonsType.Close, 
                                                Catalog.GetString("Failed to connect to file sharing service.")))
                {
                    errorDialog.WindowPosition = WindowPosition.Center;
                    errorDialog.Run();
                    errorDialog.Destroy();
                }
                
                return null;
            }
            return dbusService;
        }
        public void Start(string[] args)
        {
        
            Application.Init("gshare-manager", ref args);
            
            Catalog.Init(GShare.Constants.I18N_DOMAIN,GShare.Constants.I18N_DIR);
            
            iProgram = new Gnome.Program("gshare",String.Format("{0:0.00}",Utility.GShareVersion),Gnome.Modules.UI,args);
            
            Glade.XML xml = new Glade.XML(null, "gshare-manager.glade", "iMainWindow", GShare.Constants.I18N_DOMAIN);
            xml.Autoconnect(this);

            iDBusService = GetDBusService();
            if (iDBusService == null)
            {
                return;
            }
            else
            {
            
                double daemonVersion = iDBusService.GetDaemonVersion();
                if (daemonVersion < Utility.GShareVersion)
                {
                    /* reload daemon */
                    Log.DebugMessage("Restarting daemon");
                    iDBusService.Shutdown();
                    iDBusService = GetDBusService();
                    Log.DebugMessage("Daemon restarted");
                    if (iDBusService == null)
                        return;
                }
                
            }
            
            iMainWindow.Show();
            ResetUIState();

            if (iDBusService.IsFirstTime())
            { 

                using(Gtk.MessageDialog welcomeNote = new Gtk.MessageDialog(iMainWindow,
                          DialogFlags.Modal,
                          MessageType.Info,
                          ButtonsType.Close,
                          Catalog.GetString("Because this is the first time you're running GShare a 'Shared Files' directory has been created in your home folder.\n\nTo share files with other users, you must link or copy the files to this directory.")))
                {
                    welcomeNote.WindowPosition = WindowPosition.CenterOnParent;
                    welcomeNote.TransientFor = iMainWindow;
                    
                    welcomeNote.Run();
                    welcomeNote.Destroy();
                }
                
            }
            

            Application.Run();

        }

        public static void Main(string[] args)
        {            
            new GShareManager().Start(args);
        }

    }
}
