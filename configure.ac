AC_INIT(README)
AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE(gshare, 0.94)
AM_MAINTAINER_MODE

#AM_GNU_GETTEXT([external])

AC_PROG_INSTALL
AC_PROG_INTLTOOL([0.23])

GETTEXT_PACKAGE=gshare
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE")

ALL_LINGUAS="ca de fr nb nl nn pl pt_BR pt sv"
AM_GLIB_GNU_GETTEXT

#gsharelocaledir='${prefix}/${DATADIRNAME}/locale'
#AC_SUBST(gsharelocaledir)

dnl
dnl check availability of pkg-config
dnl
AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
if test "x$PKG_CONFIG" = "xno"; then
	AC_MSG_ERROR([You need to install pkg-config])
fi

dnl
dnl check availability of gconftool-2
dnl 
AC_PATH_PROG(GCONFTOOL,gconftool-2)


dnl
dnl check which C# compiler to use
dnl
AC_PATH_PROG(CSC, csc, no)
AC_PATH_PROG(MCS, gmcs, no)
AC_PATH_PROG(MONO, mono, no)

CS="C#"
if test "x$CSC" = "xno" -a "x$MCS" = "xno"  ; then
	dnl AC_MSG_ERROR([You need to install a C# compiler])
	AC_MSG_ERROR([No $CS compiler found])
fi

if test "x$MCS" = "xno" ; then
	MCS=$CSC
fi

if test "x$MONO" = "xno"; then
	AC_MSG_ERROR([No mono runtime found])
fi

AC_SUBST(MCS)

dnl
dnl Mono stack dependencies
dnl the single purpose of this is to ensure that minimum versions are met
dnl

MONO_REQUIRED_VERSION=1.1.13
PKG_CHECK_MODULES(MONO, mono >= $MONO_REQUIRED_VERSION)

AC_MSG_CHECKING([for mono.pc])
if test -z `$PKG_CONFIG --variable=prefix mono`; then
  AC_MSG_ERROR([missing the mono.pc file, usually found in the mono-devel package])
else
  AC_MSG_RESULT([found])
fi

mono_deps="Mono.Posix"
for lib in $mono_deps; do
  AC_MSG_CHECKING([for $lib.dll])
  if test ! -e `$PKG_CONFIG --variable=prefix mono`/lib/mono/1.0/$lib.dll; then
    AC_MSG_ERROR([missing required Mono DLL: $lib.dll])
  else
    AC_MSG_RESULT([found])
  fi
done

PKG_CHECK_MODULES(GTKSHARP, gtk-sharp-2.0 >= 2.8)
PKG_CHECK_MODULES(GCONFSHARP, gconf-sharp-2.0 >= 2.8)
PKG_CHECK_MODULES(GLADESHARP, glade-sharp-2.0 >= 2.8)
PKG_CHECK_MODULES(GNOMESHARP, gnome-sharp-2.0 >= 2.8)
PKG_CHECK_MODULES(AVAHISHARP, avahi-sharp >= 0.6.5)

dnl
dnl Check which of our own compilation flags are used
dnl 

AC_MSG_CHECKING([if debug enabled])
AC_ARG_ENABLE(debug,AC_HELP_STRING([--enable-debug],[enable debug output]),[enable_debug=yes],[enable_debug=no])
AC_MSG_RESULT($enable_debug)
if test "x$enable_debug" = xyes; then
  CS_DEFINES=-define:DEBUG
  MCS_OPTIONS=-debug

  AC_SUBST(CS_DEFINES)
  AC_SUBST(MCS_OPTIONS)
fi

AC_MSG_CHECKING([what is the dbus service directory])
AC_ARG_WITH(dbus-service-dir,
            AC_HELP_STRING([--with-dbus-service-dir=DIR],
                            [Specify the directory of DBus services (default: DATADIR/dbus-1/services)]),
            [DBUS_SERVICE_DIR=$withval])

if test "x$DBUS_SERVICE_DIR" = x; then
  DBUS_SERVICE_DIR="$datadir/dbus-1/services"
fi
AC_MSG_RESULT($DBUS_SERVICE_DIR)
AC_SUBST(DBUS_SERVICE_DIR)

dnl
dnl Run GConf macro
dnl

AM_GCONF_SOURCE_2


dnl
dnl All done, write Makefiles
dnl
AC_OUTPUT([ Makefile
data/Makefile
src/Makefile
src/ndesk/Makefile
src/gsharelib/Makefile
src/gshared/Makefile
src/gshare-manager/Makefile
src/resources/Makefile
po/Makefile.in
help/Makefile
help/C/Makefile
])

dnl Configuration summary
echo ""
echo "Configuration summary"
echo ""
echo "   * Installation prefix = $prefix"
echo "   * $CS compiler: $MCS"
echo "   * DBus service directory = $DBUS_SERVICE_DIR"
echo "   * Debug enabled: $enable_debug"
echo ""

