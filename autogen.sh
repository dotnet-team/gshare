#! /bin/sh

PROJECT=gshare
CONFIGURE=configure.ac

: ${AUTOCONF=autoconf}
: ${AUTOHEADER=autoheader}
: ${AUTOMAKE=automake}
: ${LIBTOOLIZE=libtoolize}
: ${ACLOCAL=aclocal}
: ${LIBTOOL=libtool}

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

ORIGDIR=`pwd`
cd $srcdir
TEST_TYPE=-f
aclocalinclude="-I . $ACLOCAL_FLAGS"

DIE=0

($AUTOCONF --version) < /dev/null > /dev/null 2>&1 || {
        echo
        echo "You must have autoconf installed to compile $PROJECT."
        echo "Download the appropriate package for your distribution,"
        echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
        DIE=1
}

($AUTOMAKE --version) < /dev/null > /dev/null 2>&1 || {
        echo
        echo "You must have automake installed to compile $PROJECT."
        echo "Get ftp://sourceware.cygnus.com/pub/automake/automake-1.4.tar.gz"
        echo "(or a newer version if it is available)"
        DIE=1
}

(grep "^AM_PROG_LIBTOOL" $CONFIGURE >/dev/null) && {
  ($LIBTOOL --version) < /dev/null > /dev/null 2>&1 || {
    echo
    echo "**Error**: You must have \`libtool' installed to compile $PROJECT."
    echo "Get ftp://ftp.gnu.org/pub/gnu/libtool-1.2d.tar.gz"
    echo "(or a newer version if it is available)"
    DIE=1
  }
}

if grep "^AM_[A-Z0-9_]\{1,\}_GETTEXT" "$CONFIGURE" >/dev/null; then
 if grep "sed.*POTFILES" "$CONFIGURE" >/dev/null; then
  GETTEXTIZE=""
 else
  if grep "^AM_GLIB_GNU_GETTEXT" "$CONFIGURE" >/dev/null; then
    GETTEXTIZE="glib-gettextize"
  else
    GETTEXTIZE="gettextize"
  fi

  $GETTEXTIZE --version < /dev/null > /dev/null 2>&1
  if test $? -ne 0; then
    echo
    echo "**Error**: You must have \`$GETTEXTIZE' installed" \
         "to compile $PKG_NAME."
    DIE=1
  fi
 fi
fi

#(grep "^AC_PROG_INTLTOOL" "$CONFIGURE" >/dev/null) && {
#(intltoolize --version) < /dev/null > /dev/null 2>&1 || {
# echo
# echo "**Error**: You must have \`intltoolize' installed" \
#      "to compile $PKG_NAME."
# DIE=1
# }
#}

if test "$DIE" -eq 1; then
        exit 1
fi
                                                                                
if test -z "$*"; then
        echo "I am going to run ./configure with no arguments - if you wish "
        echo "to pass any to it, please specify them on the $0 command line."
fi

[[ -f "Makefile" ]] && make distclean

case $CC in
*xlc | *xlc\ * | *lcc | *lcc\ *) am_opt=--include-deps;;
esac

if test "$GETTEXTIZE"; then
    echo "Creating $dr/aclocal.m4 ..."
    test -r aclocal.m4 || touch aclocal.m4
    echo "Running $GETTEXTIZE...  Ignore non-fatal messages."
    echo "no" | $GETTEXTIZE --force --copy
    echo "Making aclocal.m4 writable ..."
    test -r aclocal.m4 && chmod u+w aclocal.m4
fi
#if grep "^AC_PROG_INTLTOOL" $bn >/dev/null; then
#    echo "Running intltoolize..."
    intltoolize --copy --force --automake
#fi

(grep "^AM_PROG_LIBTOOL" $CONFIGURE >/dev/null) && {
    echo "Running $LIBTOOLIZE ..."
    $LIBTOOLIZE --force --copy
}

echo "Running $ACLOCAL $aclocalinclude ..."
$ACLOCAL $aclocalinclude

echo "Running $AUTOMAKE --gnu $am_opt ..."
$AUTOMAKE --add-missing --gnu $am_opt

echo "Running $AUTOCONF ..."
$AUTOCONF

echo Running $srcdir/configure $conf_flags "$@" ...
$srcdir/configure --enable-maintainer-mode $conf_flags "$@" \

